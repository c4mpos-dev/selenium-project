import time

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

service = Service(ChromeDriverManager().install())

navegador = webdriver.Chrome(service=service)

navegador.get('https://www.python.org/')

navegador.find_element('xpath', '//*[@id="documentation"]/a').click()

time.sleep(2)

navegador.find_element('xpath', '//*[@id="id-search-field"]').send_keys("classes")

time.sleep(2)

navegador.find_element('xpath', '//*[@id="submit"]').click()

time.sleep(20)